FROM ubuntu:latest

RUN apt-get update && apt-get install -y wget sudo

RUN wget -q https://get.coollabs.io/coolify/install.sh -O install.sh
RUN sudo bash ./install.sh

CMD ["/bin/bash"]
